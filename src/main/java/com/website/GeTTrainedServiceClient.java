package com.website;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.transferclasses.PostFinishPersonalAnswer;
import com.transferclasses.PostPausePersonalAnswer;
import com.transferclasses.PostPersonalAnswers;
import com.transferclasses.PostPersonalTutorial;
import com.transferclasses.PostResult;
import com.transferclasses.ToPersTutorial;
import com.transferclasses.ToPersTutorials;
import com.transferclasses.ToPersonalAnswers;
import com.transferclasses.ToPersonalTutorialResult;
import com.transferclasses.ToQuestions;
import com.transferclasses.ToTutorial;
import com.transferclasses.ToTutorialImage;
import com.transferclasses.ToTutorialMaterials;
import com.transferclasses.ToTutorialVideo;
import com.transferclasses.ToTutorials;

@FeignClient(name="GETTrained")
public interface GeTTrainedServiceClient {
	@RequestMapping(method=RequestMethod.GET, value="/employee/getTutorials/{employee_id}", consumes="application/json")
	ResponseEntity<ToTutorials> getTutorialsToEmployee(@PathVariable("employee_id") int employee_id);
	
	@RequestMapping(method=RequestMethod.GET, value="/getTutorial/{tutorial_id}", consumes="application/json")
	ResponseEntity<ToTutorial> getTutorialDataToTutorialId(@PathVariable("tutorial_id") int tutorial_id);

	@RequestMapping(method=RequestMethod.GET, value="/employee/getPersTutorials/{employee_id}", consumes="application/json")
	ResponseEntity<ToPersTutorials> getPersonalTutorialsToEmployee(@PathVariable("employee_id") int employee_id);

	@RequestMapping(method=RequestMethod.GET, value="/tutorial/material/{tutorial_id}", consumes="application/json")
	ResponseEntity<ToTutorialMaterials> getMaterialToTutorial(@PathVariable("tutorial_id") int tutorial_id);

	@RequestMapping(method=RequestMethod.GET, value="/tutorial/material/image/{material_id}", consumes="application/json")
	ResponseEntity<ToTutorialImage> getImageToMaterial(@PathVariable("material_id") int material_id);
	
	@RequestMapping(method=RequestMethod.GET, value="/tutorial/material/video/{material_id}", consumes="application/json")
	ResponseEntity<ToTutorialVideo> getVideoToMaterial(@PathVariable("material_id") int material_id);
	
	@RequestMapping(method=RequestMethod.GET, value="/tutorial/questions/{tutorial_id}", consumes="application/json")
	ResponseEntity<ToQuestions> getQuestionsToTutorial(@PathVariable("tutorial_id") int tutorial_id);
	
	@RequestMapping(method=RequestMethod.GET, value="/employee/personalanswer/{persTut_id}", consumes="application/json")
	ResponseEntity<ToPersonalAnswers> getPersonalAnswersToPersTut(@PathVariable("persTut_id") int persTut_id);
	
	@RequestMapping(method=RequestMethod.GET, value="/employee/personaltutorial/getResult/{persTut_id}", consumes="application/json")
	ResponseEntity<ToPersonalTutorialResult> getResultToPersTut(@PathVariable("persTut_id") int persTut_id);
	
	@RequestMapping(method=RequestMethod.POST, value="/employee/addpersonaltutorial/", consumes="application/json")
	ResponseEntity<ToPersTutorial>createPersonalTutorial(@RequestBody PostPersonalTutorial postPersonalTutorial);

	@RequestMapping(method=RequestMethod.POST, value="/employee/personaltutorial/finish/", consumes="application/json")
	ResponseEntity<PostResult>postPersonalTutorialFinish(@RequestBody PostFinishPersonalAnswer postFinishPersonalAnswer);

	@RequestMapping(method=RequestMethod.POST, value="/employee/personaltutorial/pause/", consumes="application/json")
	ResponseEntity<PostResult>postPersonalTutorialPause(@RequestBody PostPausePersonalAnswer postPausePersonalAnswer);

	@RequestMapping(method=RequestMethod.POST, value="/employee/personaltutorial/addpersonalanswer/", consumes="application/json")
	ResponseEntity<PostResult>postPersonalAnswers(@RequestBody PostPersonalAnswers postPersonalAnswers);

	@RequestMapping(method=RequestMethod.GET, value="/employee/personaltutorial/getPDF/{persTut_id}",consumes="application(json")
	ResponseEntity<byte[]>getPdfToPersonalTutorial(@PathVariable("persTut_id")int perTut_id);
}
