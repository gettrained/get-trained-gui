package com.website;

import org.springframework.cloud.netflix.feign.FeignClient;

import com.transferclasses.PostFinishPersonalAnswer;
import com.transferclasses.PostPausePersonalAnswer;
import com.transferclasses.PostPersonalAnswers;
import com.transferclasses.PostPersonalTutorial;

import feign.Headers;
import feign.Param;
import feign.RequestLine;
import feign.Response;

@FeignClient(name = "GETTrained")
public interface FeignInterface {
	//@RequestLine("GET /employee/getTutorials/{employee_id}")
	//ToTutorials getTutorialsToEmployee(@Param("employee_id")Integer employee_id);

	@RequestLine("GET /employee/getTutorials/{employee_id}")
	Response getTutorialsToEmployeeResponse(@Param("employee_id")Integer employee_id);
	
	@RequestLine("GET /getTutorial/{tutorial_id}")
	Response getTutorialDataToTutorialIdResponse(@Param("tutorial_id") Integer tutorial_id);
	
	//@RequestLine("GET /employee/getPersTutorials/{employee_id} ")
	//ToPersTutorials getPersonalTutorialsToEmployee(@Param("employee_id")Integer employee_id);
	
	@RequestLine("GET /employee/getPersTutorials/{employee_id} ")
	Response getPersonalTutorialsToEmployeeResponse(@Param("employee_id")Integer employee_id);
	
	
	//@RequestLine("GET /tutorial/material/{tutorial_id}")
	//ToTutorialMaterials getMaterialsToTutorial(@Param("tutorial_id")Integer tutorial_id);
	
	@RequestLine("GET /tutorial/material/{tutorial_id}")
	Response getMaterialsToTutorialResponse(@Param("tutorial_id")Integer tutorial_id);
	
	//@RequestLine("GET /tutorial/material/image/{material_id}")
	//ToTutorialImage getImageToMaterial(@Param("material_id")Integer material_id);
	
	@RequestLine("GET /tutorial/material/image/{material_id}")
	Response getImageToMaterialResponse(@Param("material_id")Integer material_id);
	
	//@RequestLine("GET /tutorial/material/video/{material_id}")
	//ToTutorialVideo getVideoToMaterial(@Param("material_id") Integer material_id);
	
	@RequestLine("GET /tutorial/material/video/{material_id}")
	Response getVideoToMaterialResponse(@Param("material_id") Integer material_id);
	
	//@RequestLine("GET /tutorial/questions/{tutorial_id}")
	//ToQuestions getQuestionsToTutorial(@Param("tutorial_id") Integer tutorial_id);
	
	@RequestLine("GET /tutorial/questions/{tutorial_id}")
	Response getQuestionsToTutorialResponse(@Param("tutorial_id") Integer tutorial_id);
	
	//@RequestLine("GET /employee/personalanswer/{persTut_id}")
	//ToPersonalAnswers getPersonalAnswersToPersTut(@Param("persTut_id")Integer persTut_id);
		
	@RequestLine("GET /employee/personalanswer/{persTut_id}")
	Response getPersonalAnswersToPersTutResponse(@Param("persTut_id")Integer persTut_id);
	
	@RequestLine("GET /employee/personaltutorial/getResult/{persTut_id}")
	Response getResultToPersTutIDResponse(@Param("persTut_id")Integer persTut_id);
	
	
	//@RequestLine("POST /employee/addpersonaltutorial")
	//@Headers("Content-Type: application/json")
	//ToPersTutorial createPersonalTutorial(PostPersonalTutorial postPersonalTutorial);

	@RequestLine("POST /employee/addpersonaltutorial")
	@Headers("Content-Type: application/json")
	Response createPersonalTutorialResponse(PostPersonalTutorial postPersonalTutorial);

	//@RequestLine("POST /employee/personaltutorial/finish")
	//@Headers("Content-Type: application/json")
	//PostResult postPersonalTutorialFinish(PostFinishPersonalAnswer finishPersonalAnswer);

	@RequestLine("POST /employee/personaltutorial/finish")
	@Headers("Content-Type: application/json")
	Response postPersonalTutorialFinishResponse(PostFinishPersonalAnswer finishPersonalAnswer);

	//@RequestLine("POST /employee/personaltutorial/pause")
	//@Headers("Content-Type: application/json")
	//PostResult postPersonalTutorialPause(PostPausePersonalAnswer postPausePersonalAnswer);
	
	@RequestLine("POST /employee/personaltutorial/pause")
	@Headers("Content-Type: application/json")
	Response postPersonalTutorialPauseResponse(PostPausePersonalAnswer postPausePersonalAnswer);
	
	//@RequestLine("POST /employee/personaltutorial/addpersonalanswer")
	//@Headers("Content-Type: application/json")
	//PostResult postPersonalAnswers(PostPersonalAnswers postPersonalAnswers );
	
	@RequestLine("POST /employee/personaltutorial/addpersonalanswer")
	@Headers("Content-Type: application/json")
	Response postPersonalAnswersResponse(PostPersonalAnswers postPersonalAnswers );

	
}
