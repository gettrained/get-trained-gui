package com.website;

import org.springframework.ui.Model;

import com.netflix.client.ClientException;

public class Strings {
	//String f�r die �bersicht der Tutorials
	public static final String overview_tabbar_h4="Available Tutorials";
	public static final String overview_tabbar_tab_open="Open";
	public static final String overview_tabbar_tab_partial="Partial";
	public static final String overview_tabbar_tab_done="Done";
	public static final String overview_tabbar_tab_all="All";
	//String f�r die Buttons f�r die �bersicht der Tutorials
	public static final String overview_button_load_tutorial="load Tutorial";
	public static final String overview_button_start_tutorial="start Tutorial";
	public static final String overview_button_continue_tutorial="continue Tutorial";
	public static final String overview_button_view_result="Result";
	//String f�r das Modal(Popup-Fenster) 
	public static final String modal_title_information="Information";
	public static final String modal_title_error="Fehler";
	public static final String modal_button_close="Schlie�en";
	public static final String modal_button_overview="zur�ck zur �bersicht";
	public static final String modal_existing_partial_tutorial="F�r dieses Tutorial ist bereits ein teilweise fertiges Tutorial vorhanden";
	public static final String modal_no_answer_selected="Eine Antwort muss mindestens ausgew�hlt werden";
	public static final String modal_watch_full_video="Sie m�ssen das gesamte Video ansehen, um weiter zu gehen";	
	public static final String modal_creating_pdf="Bitte warten. Die PDF wird erstellt";
	//Fehlermeldungen
	public static final String error_no_available_server="Es konnte keine Verbindung zum Server aufgebaut werden.";
	//String f�r die PersTut Darstellung unter Done
	public static final String overview_tab_done_walkthrough_part_1="erledigt am: ";
	public static final String overview_tab_done_walkthrough_part_2=" - ";
	public static final String overview_tab_done_walkthrough_part_3="/";
	public static final String overview_tab_done_walkthrough_part_4=" Fragen korrekt beantwortet.";
	//String f�r Material
	public static final String material_headline_1="Material ";
	public static final String material_headline_2=" von ";
	public static final String material_alt_image="Es gab einen Fehler beim Anzeigen des Bildes.";
	//String f�r Start der Fragen
	public static final String startOfQuestion_headline="Ende des Materials";
	public static final String startOfQuestion_text="Wenn Sie nun weitergehen, beginnen die Fragen und Sie k�nnen nicht zum Material zur�ck.";
	//String f�r Frage
	public static final String question_headline_1="Frage ";
	public static final String question_headline_2=" von ";
	public static final String question_text_no_answer_selected="Sie m�ssen eine Antwort ausw�hlen.";
	public static final String question_alt_image="Es gab einen Fehler beim Anzeigen des Bildes.";
	//String f�r Ende der Fragen
	public static final String endOfQuestion_headline="Ende der Fragen";
	public static final String endOfQuestion_text="Wenn Sie nun weitergehen, schlie�en Sie das Tutorial ab und Sie erhalten Ihre Auswertung.";
	//String f�r Auswertung
	public static final String score_headline="Ergebnis";
	public static final String score_total_points_1="";
	public static final String score_total_points_2="-";
	public static final String score_total_points_3="";
	public static final String score_succeeded="Bestanden";
	public static final String score_not_succeeded="Nicht Bestanden";
	public static final String score_needed_points_1="Sie ben�tigten ";
	public static final String score_needed_points_2=" richtig beantwortete Fragen, um das Tutorial zu bestehen";
	
	public static Model includeStrings(Model model) {
		model.addAttribute("overview_tabbar_h4",overview_tabbar_h4);
		model.addAttribute("overview_tabbar_tab_open",overview_tabbar_tab_open);
		model.addAttribute("overview_tabbar_tab_partial",overview_tabbar_tab_partial);
		model.addAttribute("overview_tabbar_tab_done",overview_tabbar_tab_done);
		model.addAttribute("overview_tabbar_tab_all",overview_tabbar_tab_all);
		
		model.addAttribute("overview_button_load_tutorial",overview_button_load_tutorial);
		model.addAttribute("overview_button_start_tutorial",overview_button_start_tutorial);
		model.addAttribute("overview_button_continue_tutorial",overview_button_continue_tutorial);
		model.addAttribute("overview_button_view_result",overview_button_view_result);
		
		model.addAttribute("modal_title_information",modal_title_information);
		model.addAttribute("modal_title_error", modal_title_error);
		model.addAttribute("modal_button_close",modal_button_close);
		model.addAttribute("modal_button_overview",modal_button_overview);
		model.addAttribute("modal_existing_partial_tutorial",modal_existing_partial_tutorial);
		model.addAttribute("modal_no_answer_selected", modal_no_answer_selected);
		model.addAttribute("modal_watch_full_video", modal_watch_full_video);
		model.addAttribute("modal_creating_pdf", modal_creating_pdf);
		
		model.addAttribute("error_no_available_server", error_no_available_server);
		
		model.addAttribute("material_headline_1", material_headline_1);
		model.addAttribute("material_headline_2",material_headline_2);
		model.addAttribute("material_alt_image",material_alt_image);
		
		model.addAttribute("startOfQuestion_headline", startOfQuestion_headline);
		model.addAttribute("startOfQuestion_text",startOfQuestion_text);
		
		model.addAttribute("question_headline_1",question_headline_1);
		model.addAttribute("question_headline_2",question_headline_2);
		model.addAttribute("question_text_no_answer_selected",question_text_no_answer_selected);
		
		model.addAttribute("endOfQuestion_headline",endOfQuestion_headline);
		model.addAttribute("endOfQuestion_text",endOfQuestion_text);
		
		model.addAttribute("score_headline",score_headline);
		model.addAttribute("score_total_points_1",score_total_points_1);
		model.addAttribute("score_total_points_2",score_total_points_2);
		model.addAttribute("score_total_points_3",score_total_points_3);
		model.addAttribute("score_succeeded",score_succeeded);
		model.addAttribute("score_not_succeeded",score_not_succeeded);
		model.addAttribute("score_needed_points_1",score_needed_points_1);
		model.addAttribute("score_needed_points_2",score_needed_points_2);
		return model;
	}
}
