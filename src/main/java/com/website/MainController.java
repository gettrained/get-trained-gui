package com.website;


import java.util.Date;
import java.util.Calendar;
import java.util.LinkedList;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.classes.Answer;
import com.classes.Material;
import com.classes.Question;
import com.classes.Result;
import com.classes.Tutorial;
import com.comparator.ComparatorToPersTutorial;
import com.sun.jersey.core.util.Base64;
import com.transferclasses.*;

import feign.Param;


@Controller
@EnableAutoConfiguration
@EnableEurekaClient
@EnableFeignClients
public class MainController {
	
	private static int employee_id=7;
	
	@Autowired
	private GeTTrainedServiceClient getTrainedServiceClient;
	private Tutorial tutorial;//current loaded tutorial
	private Result result;//current loaded result
	
	
	@RequestMapping("/loadTutorial_body")
	@ResponseBody
	public String loadTutorial_body(@Param("tutorial_id")Integer tutorial_id,@Param("group_number")Integer group_number) {
		try {			
			ResponseEntity<ToTutorials>re_toTutorials=getTrainedServiceClient.getTutorialsToEmployee(employee_id);
			
			if(re_toTutorials.getStatusCode()==HttpStatus.OK) {
				try {
						tutorial=new Tutorial(re_toTutorials.getBody().getToTutorialById(tutorial_id), "");
					//Load materials
						ResponseEntity<ToTutorialMaterials>re_toTutorialMaterial=getTrainedServiceClient.getMaterialToTutorial(tutorial_id);
						try {
							if(re_toTutorialMaterial.getStatusCode()==HttpStatus.OK) {
								for(ToTutorialMaterial toMaterial :re_toTutorialMaterial.getBody().getMaterials()) {
									switch(toMaterial.getMaterialtype().toUpperCase()) {
										case "VIDEO":
											ResponseEntity<ToTutorialVideo>re_toTutorialVideo=getTrainedServiceClient.getVideoToMaterial(toMaterial.getMaterialID());
											tutorial.addMaterial(new Material(toMaterial.getMaterialtype(),toMaterial.getOrder(),"",re_toTutorialVideo.getBody().getVideo()));
											break;
										case "IMAGE":
											ResponseEntity<ToTutorialImage>re_toTutorialImage=getTrainedServiceClient.getImageToMaterial(toMaterial.getMaterialID());
											tutorial.addMaterial(new Material(toMaterial.getMaterialtype(), toMaterial.getOrder(),re_toTutorialImage.getBody().getText(),re_toTutorialImage.getBody().getByteStream()));
											break;
										default: return "Unknown materialtype:"+toMaterial.getMaterialtype().toUpperCase();
									}
								}
							}else {
								return "HttpStatus is not OK:"+re_toTutorialMaterial.getStatusCodeValue();
							}
						}catch (NullPointerException npex) {
							if(re_toTutorialMaterial.getBody()==null) {
								return "re_toTutorialMaterial is null";
							}
							return "Error while loading the material"+npex.getMessage();
						}
					//Load Questions with Answers
						ResponseEntity<ToQuestions>re_toQuestions=getTrainedServiceClient.getQuestionsToTutorial(tutorial_id);
						try {
							if(re_toQuestions.getStatusCode()==HttpStatus.OK) {
								for(ToQuestion toQuestion:re_toQuestions.getBody().getQuestions()) {
									LinkedList<Answer>answers=new LinkedList<>();
									for(ToAnswer toAnswer:toQuestion.getAnswers()) {
										answers.add(new Answer(toAnswer.getId(),false,toAnswer.getAnswerText(),toAnswer.isCorrect()));
									}
									tutorial.addQuestion(new Question(answers,toQuestion.getText(),toQuestion.getOrder(),toQuestion.getByteStream()));
								}								
							}else {
								return "HttpStatus is not OK:"+re_toQuestions.getStatusCodeValue();
							}
						}catch(NullPointerException npex) {
							if(re_toQuestions.getBody()==null) {
								return "re_toQuestion is null";
							}
							return "Error while loading the questions with answers"+npex.getMessage();
						}
				}catch ( NullPointerException npex) {
					if(re_toTutorials.getBody()==null) {
						return "re_toTutorials is null";
					}
					return "NullPointerException "+npex.getMessage(); 
				}
			}else {
				return "HttpStatus ist not OK:"+re_toTutorials.getStatusCodeValue();
			}
		}catch(Exception ex) {
			
			return ex.getMessage();
		}
		return "successful";
	}
	
	@RequestMapping("/startTutorial")
	public String startTutorial(Model model){
		model=Strings.includeStrings(model);
		try {
			if(tutorial!=null) {
				ResponseEntity<ToPersTutorial> re_toPersTutorial=getTrainedServiceClient.createPersonalTutorial(new PostPersonalTutorial(employee_id, tutorial.getId()));
				if(re_toPersTutorial.getStatusCode()==HttpStatus.CREATED) {
					try {
						tutorial.setPerstut_id(re_toPersTutorial.getBody().getId());
						tutorial.setUsedTime(re_toPersTutorial.getBody().getUsedtime());;
						tutorial.setStartTutorialUser(new Date());	//jetziger Zeitpunkt, da ja die wirklich ben�tigte Zeit ben�tigt wird und nicht die Differenz zwischen DateCreated und DateFinished	
						tutorial.setLastSeen(re_toPersTutorial.getBody().getLastseen());
						return getMaterial(tutorial.getLastSeen_Order(), model);
					}catch(NullPointerException npex) {
						model.addAttribute("error_text","NullpointerException"+npex.getMessage());
						return "Error";
					}				
				}else {
					model.addAttribute("error_text","HTTP responsePersTut is not CREATED: "+re_toPersTutorial.getStatusCodeValue());
					return "Error";
				}
			}else {
				model.addAttribute("error_text","Error: Tutorial is null ");
				return "Error";
				//getTutorial(model);
			}
		}catch(Exception ex) {
			model.addAttribute("error_text",ex.getMessage());
			return "Error";
		}		
	}
	
	
	@RequestMapping("/continueTutorial")
	public String continueTutorial(Model model) {
		model=Strings.includeStrings(model);
		try {
			ResponseEntity<ToPersTutorials>re_personalTutorials=getTrainedServiceClient.getPersonalTutorialsToEmployee(employee_id);
			 
			try {
				if(re_personalTutorials.getStatusCode()==HttpStatus.OK) {
					ToPersTutorial personalTutorial=re_personalTutorials.getBody().getPartialPersTutorialByTutorialID(tutorial.getId());
					tutorial.setPerstut_id(personalTutorial.getId());
					tutorial.setUsedTime(personalTutorial.getUsedtime());
					tutorial.setStartTutorialUser(new Date());//jetziger Zeitpunkt, da ja die wirklich gebrachte Zeit vorhanden sein sollte und nicht die Differenz zwischen DateCreated und DateFinished			
					tutorial.setLastSeen(personalTutorial.getLastseen());
					ResponseEntity<ToPersonalAnswers> re_personalAnswers=getTrainedServiceClient.getPersonalAnswersToPersTut(tutorial.getPerstut_id());
					try {
						if(re_personalAnswers.getStatusCode()==HttpStatus.OK) {
							if(re_personalAnswers.getBody().getPersonalanswers().size()>0) {
								//holen der AnswerID der letzten selektieren Antwort.
								int orderQuestion=-1;
								for(int i=0;i<re_personalAnswers.getBody().getPersonalanswers().size();i++) {
									int newOrder=tutorial.setPersonalAnswer(re_personalAnswers.getBody().getPersonalanswers().get(i));//allen Vorhandenen Antworten den jeweiligen Wert setzen
									if(re_personalAnswers.getBody().getPersonalanswers().get(i).isSelected()&&newOrder>orderQuestion) {
										orderQuestion=newOrder;
									}
								}
								if(orderQuestion>-1) {//Wenn Fragen schon beantwortet
									return getQuestion(orderQuestion, model);										
								}else {//wenn noch keine selektiert worden sind
									//wird die erste Frage angezeigt
									return getQuestion(1,model);
								}	
							}else {//wenn keine PersonalAnswers vorhanden sind
								//wird das Material angezeigt.
								
								return getMaterial(tutorial.getLastSeen_Order(), model);
							}
						}else {
							model.addAttribute("error_text","PersonalAnswer Httpstatus is not OK: "+re_personalAnswers.getStatusCodeValue());
							return "Error";
						}
					}catch(NullPointerException npex) {						
						if(re_personalAnswers.getBody()==null) {
							model.addAttribute("error_text","PersonalAnswers is null");
							return "Error";
						}
						model.addAttribute("error_text", "Fehler beim Laden der Antworten");
						return "Error";
					}
				}else {
					model.addAttribute("error_text","HttpStatus re_personalTutorials is not OK: "+re_personalTutorials.getStatusCodeValue());
					return "Error";
				}
			}catch(NullPointerException npex) {
				if(tutorial==null) {
					model.addAttribute("error_text","Tutorial ist null");
					return "Error";			
				}
				if(re_personalTutorials.getBody()==null) {
					model.addAttribute("error_text","re_personalTutorials is null");
					return "Error";
				}
				model.addAttribute("error_text","Fehler beim Laden des Tutorials");
				return "Error";
			}
		}catch(Exception ex) {
			model.addAttribute("error_text",ex.getMessage());
			return "Error";
		}		
	}
		
	
	
	@RequestMapping("/tutorials")
	public String getTutorials(Model model) {//anzeigen der �bersicht
		model=Strings.includeStrings(model);
		try {
			ResponseEntity<ToTutorials>re_toTutorials=getTrainedServiceClient.getTutorialsToEmployee(employee_id);
			if(re_toTutorials.getStatusCode()==HttpStatus.OK) {
				try {
					ResponseEntity<ToPersTutorials>re_toPersTutorial=getTrainedServiceClient.getPersonalTutorialsToEmployee(employee_id);
					if(re_toPersTutorial.getStatusCode()==HttpStatus.OK) {
						try {
							model=fillShownTutorial(re_toTutorials.getBody(), re_toPersTutorial.getBody(), model);
							try {
								model=fillNumberOfTutorial(re_toTutorials.getBody(), model);
							}catch(Exception ex) {
								model.addAttribute("error_text",ex.getMessage());
								return "Error";
							}
							if(!model.containsAttribute("loadedTutorialId")) {
								model.addAttribute("loadedTutorialId",-1);
								model.addAttribute("isLoaded",false);
							}else {
								model.addAttribute("isLoaded",true);
							}
							return "All_Tutorials";
						}catch(NullPointerException npex) {
							if(re_toPersTutorial.getBody()==null) {
								model.addAttribute("error_text","re_toPersTutorial is null");
								return "Error";
							}
							model.addAttribute("error_text",npex.getMessage());
							return "Error";
						}
					}else {
						model.addAttribute("error_text","re_toPersTutorial is not OK:"+re_toPersTutorial.getStatusCodeValue());
						return "Error";
					}
				}catch (NullPointerException npex) {
					if(re_toTutorials.getBody()==null) {
						model.addAttribute("error_text","re_toTutorials is null");
						return "Error";
					}
					model.addAttribute("error_text",npex.getMessage());
					return "Error";
				}
			}else {
				model.addAttribute("error_text","re_totutorials is not OK:"+re_toTutorials.getStatusCodeValue());
				return "Error";
			}
			
		}catch(Exception ex) {
			model.addAttribute("error_text", ex.getMessage());
			return "Error";
		}
	}
	
	@RequestMapping("/material")
	public String getMaterial(@Param("order") Integer order, Model model) {
		model=Strings.includeStrings(model);
		Material material=tutorial.getMaterialByOrder(order);
		if(material!=null) {
			model.addAttribute("tutorial", tutorial);
			model.addAttribute("material", material);
			model.addAttribute("numberOfMaterials",tutorial.getMaterial().size());
			
			if(material.getMaterialtype().equals("IMAGE")) {				
				return "Material_Image";
			}else {
				model.addAttribute("lastSeen", tutorial.getLastSeen_Seconds());
				return "Material_Video";
			}
		}else {
			model.addAttribute("tutorial",tutorial);
			model.addAttribute("order",order);
			return "StartQuestions";
		}		
	}
		
	@RequestMapping("/question")
	public String getQuestion(@Param("order")Integer order, Model model) {
		model=Strings.includeStrings(model);
		Question question=tutorial.getQuestionByOrder(order);	
		if(question!=null) {
			model.addAttribute("question",question);	
			model.addAttribute("numberOfQuestions", tutorial.getQuestions().size());
			model.addAttribute("tutorial",tutorial);		
			return "Question";
		}else {
			model.addAttribute("tutorial",tutorial);
			model.addAttribute("order", order);
			return "EndOfQuestions";
		}
	}
	@RequestMapping("sendAnswer")
	@ResponseBody
	public boolean sendAnswer(@Param("answer_id")Integer answer_id, @Param("answer")Boolean answer) {
		return tutorial.setAnswerToQuestion(answer_id, answer);
	}
	
	@RequestMapping("exitMaterial")
	public String pauseTutorial(@Param("order")Integer order, @Param("currentTime")Double currentTime,Model model) {
		model=Strings.includeStrings(model);
		try {
			String lastSeen;
			if(currentTime==null&&order!=null) {//image
				lastSeen=""+(order-1);
			}else {//video
				lastSeen=""+Math.floor(currentTime*100)/100;
			}
			PostPausePersonalAnswer postPausePersonalAnswer=new PostPausePersonalAnswer(tutorial.getPerstut_id(), tutorial.getNewUsedTime(), lastSeen);
			ResponseEntity<PostResult>re_PostResultPause=getTrainedServiceClient.postPersonalTutorialPause(postPausePersonalAnswer);
			try {
				if(re_PostResultPause.getStatusCode()==HttpStatus.NO_CONTENT) {
					tutorial=null;//Tutorial wird verlassen, daher werden die gespeicherten Daten gel�scht.
					return getTutorials(model);					
				}else if(re_PostResultPause.getStatusCode()==HttpStatus.NOT_MODIFIED){
					model.addAttribute("error_text","HttpStatus responsePostResultPause is NOT_MODIFIED");
					return "Error";
				}else {
					model.addAttribute("error_text","HttpStatus is not NO_CONTENT or NOT_MODIFIED "+re_PostResultPause.getStatusCodeValue());
					return "Error";
				}
			}catch(NullPointerException npex) {
				if(re_PostResultPause==null) {
					model.addAttribute("error_text","re_PostResultPause is null");
					return "Error";	
				}
				model.addAttribute("error_text",npex.getMessage());
				return "Error";
			}
		}catch (NullPointerException npex) {
			if(tutorial==null) {
				model.addAttribute("error_text","tutorial is null");
				return "Error";
			}
			if(order==null) {
				model.addAttribute("error_text","�bergabeparameter order ist null");
				return "Error";
			}
			if(currentTime==null) {
				model.addAttribute("error_text","�bergabeparameter currentTime ist null");
				return "Error";
			}
			model.addAttribute("error_text","Fehler beim Verlassen des Materials"+npex.getMessage());
			return "Error";
		}
	}
	@RequestMapping("exitQuestion")
	public String exitQuestion(@Param("order")Integer order,Model model) {//letzten ausgef�llten Daten werden nicht gespeichert.
		model=Strings.includeStrings(model);
		try {
			if(saveAnswers(order)) {
				ResponseEntity<PostResult> re_PostResultPause=getTrainedServiceClient.postPersonalTutorialPause(new PostPausePersonalAnswer(tutorial.getPerstut_id(), tutorial.getNewUsedTime(), "-1"));
				try {
					if(re_PostResultPause.getStatusCode()==HttpStatus.NO_CONTENT) {
						tutorial=null;
						return getTutorials(model);
					}else {
						model.addAttribute("error_text","re_PostResultPause httpstatus is not no_content:"+re_PostResultPause.getStatusCodeValue());
						return "Error";
					}
				}catch(NullPointerException npex){
					if(re_PostResultPause.getBody()==null) {
						model.addAttribute("error_text","re_PostResultPause is null");
						return "Error";
					}
					model.addAttribute("error_text",npex.getMessage());
					return "Error";
				}
			}else {
				model.addAttribute("error_text","Fehler beim Speichern der Antworten");
				return "Error";
			}
		}catch(NullPointerException npex) {
			if(tutorial==null) {
				model.addAttribute("error_text","tutorial is null");
				return "Error";
			}
			if(order==null) {
				model.addAttribute("error_text","order is null");
				return "Error";
			}
			model.addAttribute("error_text","fehler beim Speichern der Antworten");
			return "Error";
		}		
	}
	
	private boolean saveAnswers(int untilOrder) {
		try {
			PostPersonalAnswers post_answers=new PostPersonalAnswers(tutorial.getPostPersonalAnswers(untilOrder));
			ResponseEntity<PostResult>re_PostResult=getTrainedServiceClient.postPersonalAnswers(post_answers);
			if(re_PostResult.getStatusCode()==HttpStatus.CREATED) {
				return true;
			}else {
				return re_PostResult.getBody().isResult();
			}
		}catch (NullPointerException npex) {
			return false;
		}		
	}
	
	@RequestMapping("score")
	public String getScore(Model model){
		model=Strings.includeStrings(model);
		try {
			if(saveAnswers(-1)) {//Saves all answers
				PostFinishPersonalAnswer postFinishPersonalAnswer=new PostFinishPersonalAnswer(tutorial.getPerstut_id(), tutorial.getNewUsedTime());
				ResponseEntity<PostResult>re_PostResultFinish=getTrainedServiceClient.postPersonalTutorialFinish(postFinishPersonalAnswer);
				if(re_PostResultFinish.getStatusCode()==HttpStatus.NO_CONTENT) {
					model.addAttribute("tutorial",tutorial);
					model=calculateScore(model);
					
					Result result=new Result();
					result.setTutorial_color(tutorial.getColor());
					result.setTutorial_name(tutorial.getName());
					result.setPersonal_tutorial_id(tutorial.getPerstut_id());
					result.setQuestions(tutorial.getQuestions_sorted());
					model.addAttribute("result", result);
					return "Score";
				}else {
					model.addAttribute("error_text","re_PostResultFinish httpStatus is not No_Content:"+re_PostResultFinish.getStatusCodeValue());
					return "Error";
				}
			}else {
				model.addAttribute("error_text","Fehler beim Speicher der Antworten");
				return "Error";
			}
		}catch(NullPointerException npex) {
			if(tutorial==null) {
				model.addAttribute("error_text","tutorial is null");
				return "Error";
			}
			model.addAttribute("error_text","Fehler beim Anzeigen von Score");
			return "Error";
		}		
	}
	
	public Model calculateScore(Model model) {
		int countCorrectAnswers=0;
		for(Question question:tutorial.getQuestions()) {
			int answeredPerQuestionCorrect=0;
			int correctAnswersPerQuestion=0;
			for(Answer answer:question.getAnswers()) {	
				if(answer.isCorrect()) {
					correctAnswersPerQuestion++;
				}
				if(answer.isCorrect()&&answer.isSelected()) {
					answeredPerQuestionCorrect++;
				}
				if(!answer.isCorrect()&&answer.isSelected()) {//Falsche Antwort selektiert
					answeredPerQuestionCorrect--;
				}
			}
			if(answeredPerQuestionCorrect==correctAnswersPerQuestion) {
				countCorrectAnswers++;
			}
		}
		double score_percent=((double)countCorrectAnswers/tutorial.getQuestions().size())*100.0;
		model.addAttribute("succeed", (score_percent>=tutorial.getSuccespercentage()));
		model.addAttribute("neededAmountCorrectQuestions",(int)Math.ceil((tutorial.getSuccespercentage()*tutorial.getQuestions().size())/100));
		model.addAttribute("numberCorrectAnswers",countCorrectAnswers);
		model.addAttribute("numberQuestions", tutorial.getQuestions().size());

		return model;		
	}
	
	@RequestMapping("result")
	public String getResults(@Param("tutorial_id") Integer tutorial_id, @Param("persTut_id")Integer persTut_id, Model model) throws Exception {
		model=Strings.includeStrings(model);
		try {
				result=new Result();
				LinkedList<Question>result_questions=new LinkedList<>();
				if(tutorial !=null&&tutorial_id==tutorial.getId()&&persTut_id==tutorial.getPerstut_id()) {
					result.setTutorial_color(tutorial.getColor());
					result.setTutorial_name(tutorial.getName());
					result.setPersonal_tutorial_id(persTut_id);
					result_questions=tutorial.getQuestions_sorted();
				}else {
					ResponseEntity<ToTutorial>re_tutorialData=getTrainedServiceClient.getTutorialDataToTutorialId(tutorial_id);
					try {
						if(re_tutorialData.getStatusCode()==HttpStatus.OK) {
							result.setTutorial_color(re_tutorialData.getBody().getColor());
							result.setTutorial_name(re_tutorialData.getBody().getName());
							result.setPersonal_tutorial_id(persTut_id);
							ResponseEntity<ToPersonalAnswers>re_personalAnswers=getTrainedServiceClient.getPersonalAnswersToPersTut(persTut_id);
							ResponseEntity<ToQuestions>re_questions=getTrainedServiceClient.getQuestionsToTutorial(tutorial_id);
							try {
								if(re_personalAnswers.getStatusCode()==HttpStatus.OK&&re_questions.getStatusCode()==HttpStatus.OK) {
									for(ToQuestion question : re_questions.getBody().getQuestions()) {
										LinkedList<Answer>answersToQuestion=new LinkedList<>();
										for (ToPersonalAnswer personalAnswer : re_personalAnswers.getBody().getPersonalanswers()) {
											for (ToAnswer answer : question.getAnswers()) {
												if(personalAnswer.getAnswerID()==answer.getId())						
													answersToQuestion.add(new Answer(answer.getId(),personalAnswer.isSelected(),answer.getAnswerText(),answer.isCorrect()));
												}
											}
										result_questions.add(new Question(answersToQuestion,question.getText(),question.getOrder(),question.getByteStream()));
									
									}
								}
							}catch(NullPointerException npex) {
								if(re_personalAnswers.getBody()==null) {
									model.addAttribute("error_text","PersonalAnswers is null");
									return "Error";
								}
								if(re_questions.getBody()==null) {
									model.addAttribute("error_text","Questions is null");
									return "Error";
								}
								model.addAttribute("error_text","Fehler beim Holen der Fragen und  Antworten");
								return "Error";
							}
						}
					}catch (NullPointerException npex) {
						if(re_tutorialData.getBody()==null) {
							model.addAttribute("error_text", "TutorialData is null");
							return "Error";
						}
						model.addAttribute("error_text","Fehler beim Holen der Tutorialdaten");
						return "Error";
					}
				}
				result.setQuestions(result_questions);
				
			model.addAttribute("result",result);
			return "Results";
		}catch(Exception ex){
			model.addAttribute("error_text",ex.getMessage());
			return "Error";
		}		
	}
		
	@RequestMapping("getPDF")
	@ResponseBody
	public /*String*/byte[] getPDF(Model model,HttpServletResponse response) {
		ResponseEntity<byte[]>re_pdf=getTrainedServiceClient.getPdfToPersonalTutorial(result.getPersonal_tutorial_id());
		String filename=re_pdf.getHeaders().get("content-disposition").get(0);
		response.setContentType("application/pdf");		
        response.setHeader("content-disposition", "attachment;"+filename);
        
		return Base64.decode(re_pdf.getBody());
		/*if(result!=null) {
			model.addAttribute("result",result);
			model=Strings.includeStrings(model);
			//Erzeugen eines PDFs an einen bestimmten Ordner
			/*try {
				pdfGeneratorUtil.createPdf("Results_pdf", model.asMap());
			} catch (Exception e) {
				e.printStackTrace();
			}		
			return "Results_pdf";
		
			//senden an den Browser
			try {
				return pdfGeneratorUtil.downloadPdf(response,result.getTutorial_name(), "Results_pdf", model.asMap());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;*/
	}
	
	private Model fillNumberOfTutorial(ToTutorials tutorials, Model model) throws Exception//Zu Model hinzuf�gen, wie viele Tutorials pro Gruppe vorhanden sind
	{
		LinkedList<Tutorial> shownTutorials;
		if(model.asMap().containsKey("tutorials")) {
			shownTutorials=(LinkedList<Tutorial>)model.asMap().get("tutorials");
		}else {
			throw new Exception("Model enth�lt den Key \"tutorials\" nicht");
		}			
		//Number of the tutorials
		int numberOfOpen=0;
		int numberOfPartial=0;
		int numberOfDone=0;
		for(int i=0;i<shownTutorials.size();i++) {
			switch(shownTutorials.get(i).getState()) {
			case "Open":numberOfOpen++;break;
			case"Partial":numberOfPartial++;break;
			case"Done":numberOfDone++;break;
			}		
		}
		model.addAttribute("numberOfOpen",numberOfOpen);
		model.addAttribute("numberOfPartial",numberOfPartial);
		model.addAttribute("numberOfDone",numberOfDone);
		model.addAttribute("numberOfAll",numberOfOpen+numberOfPartial+numberOfDone);
		
		return model;
	}
	private Model fillShownTutorial(ToTutorials tutorials, ToPersTutorials persTutorials, Model model){//Tutorials nach den Gruppen aufteilen
		LinkedList<Tutorial> shownTutorials=new LinkedList<>();
		// Group partial
		LinkedList<ToPersTutorial>persTutorialpartial=persTutorials.getPersTutorialsPerState("Partial");
		for(int i=0;i<persTutorialpartial.size();i++) {			
			shownTutorials.add(new Tutorial(tutorials.getToTutorialById(persTutorialpartial.get(i).getTutorialId()),"Partial"));
		}
		
		//Group Done
		LinkedList<Integer>idsPersTutorialDone=persTutorials.getPersTutorialIdsPerState("Done");
		LinkedList<ToPersTutorial>persTutorialDone=persTutorials.getPersTutorialsPerState("Done");
		persTutorialDone.sort(new ComparatorToPersTutorial());
		for(int i=0;i<idsPersTutorialDone.size();i++) {
			Tutorial t=new Tutorial(tutorials.getToTutorialById(idsPersTutorialDone.get(i)), "Done");
			for(int j=0;j<persTutorialDone.size();j++) {
				if(persTutorialDone.get(j).getTutorialId()==t.getId()) {
					//holen vom Result f�r die �bersicht
					try {
						ResponseEntity<ToPersonalTutorialResult> re_personalTutorialResult=getTrainedServiceClient.getResultToPersTut(persTutorialDone.get(j).getId());
						String result_text;
						int reached_percentage=-1;
						try {
							if(re_personalTutorialResult.getStatusCode()==HttpStatus.OK) {
								result_text=re_personalTutorialResult.getBody().getCorrectAnswers()+Strings.overview_tab_done_walkthrough_part_3+re_personalTutorialResult.getBody().getTotalAnswers();
								reached_percentage=(int)(re_personalTutorialResult.getBody().getCorrectAnswers()/(double)re_personalTutorialResult.getBody().getTotalAnswers()*100);
							}else {
								result_text="Fehler: Httpstatus is not OK";
							}
						}catch(NullPointerException npex){
							result_text="Fehler beim Laden";
						}
						Date dateFinished=persTutorialDone.get(i).getDatefinished();
						
						Calendar calendar=Calendar.getInstance();
						calendar.setTime(dateFinished);
						String day=""+calendar.get(Calendar.DAY_OF_MONTH);
						if(day.length()<2) {
							day='0'+day;
						}
						String month=""+(calendar.get(Calendar.MONTH)+1);
						if(month.length()<2) {
							month='0'+month;
						}
						String text=Strings.overview_tab_done_walkthrough_part_1+								
								day+"."+month+
								"."+calendar.get(Calendar.YEAR)+" "
								+Strings.overview_tab_done_walkthrough_part_2+
								result_text+
								Strings.overview_tab_done_walkthrough_part_4;
						t.addWalkthrough(text, persTutorialDone.get(j).getId(),reached_percentage);	
						
					}catch(Exception ex) {					
						model.addAttribute("error_text","Fehler beim Laden der Resultate: "+ex.getMessage());						
					}						
				}
			}
			shownTutorials.add(t);
			if(!t.isTutorialPassed()) {
				Tutorial t_open=new Tutorial(tutorials.getToTutorialById(idsPersTutorialDone.get(i)),"Open");
				shownTutorials.add(t_open);
			}
			
		}
		//Group Open
				for(int j=0;j<tutorials.getTutorials().size();j++) {
					boolean persTutExists=false;
					for(int i=0;i<persTutorials.getPerstutorials().size();i++) {
						if(persTutorials.getPerstutorials().get(i).getTutorialId()==tutorials.getTutorials().get(j).getId()) {
							persTutExists=true;
						}				
					}
					if(!persTutExists) {
						shownTutorials.add(new Tutorial(tutorials.getTutorials().get(j),"Open"));
					}
					
				}
		model.addAttribute("tutorials", shownTutorials);
		return model;
	}	
}
