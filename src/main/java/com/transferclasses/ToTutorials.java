package com.transferclasses;

import java.io.Serializable;
import java.util.LinkedList;

public class ToTutorials implements Serializable {
	private static final long serialVersionUID = 1L;

	private LinkedList<ToTutorial> tutorials = new LinkedList<>();

	public ToTutorial getToTutorialById(int tutorial_id) {
		for(int i=0;i<tutorials.size();i++) {
			if(tutorials.get(i).getId()==tutorial_id) {
				return tutorials.get(i);
			}
		}
		return null;
	}
	
	public ToTutorials() {

	}

	public ToTutorials(LinkedList<ToTutorial> tutorials) {
		super();
		this.tutorials = tutorials;
	}

	public LinkedList<ToTutorial> getTutorials() {
		return tutorials;
	}

	public void setTutorials(LinkedList<ToTutorial> tutorials) {
		this.tutorials = tutorials;
	}

}
