package com.transferclasses;

import java.io.Serializable;

public class ToPersonalTutorialResult implements Serializable {
	private static final long serialVersionUID = 1L;

	private int correctAnswers;
	private int totalAnswers;
	
	public ToPersonalTutorialResult() {
		super();
	}
	
	public ToPersonalTutorialResult(int correctAnswers, int totalAnswers) {
		super();
		this.correctAnswers = correctAnswers;
		this.totalAnswers = totalAnswers;
	}

	public int getCorrectAnswers() {
		return correctAnswers;
	}

	public void setCorrectAnswers(int correctAnswers) {
		this.correctAnswers = correctAnswers;
	}

	public int getTotalAnswers() {
		return totalAnswers;
	}

	public void setTotalAnswers(int totalAnswers) {
		this.totalAnswers = totalAnswers;
	}
	
	
	
}
