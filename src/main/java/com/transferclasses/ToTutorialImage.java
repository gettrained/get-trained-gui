package com.transferclasses;

import java.io.Serializable;

/**
 * Created by Pascal on 05.10.2017.
 */

public class ToTutorialImage implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String text;
    private String byteStream;

    public ToTutorialImage() {
    }

    public ToTutorialImage(String text, String byteStream) {
        super();
        this.text = text;
        this.byteStream = byteStream;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getByteStream() {
        return byteStream;
    }

    public void setByteStream(String byteStream) {
        this.byteStream = byteStream;
    }
}
