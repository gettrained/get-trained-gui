package com.transferclasses;

import java.io.Serializable;

public class ToTutorial implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private int succespercentage;
	private String color;
	private String name;

	public ToTutorial(int id, int succespercentage, String color, String name) {
		super();
		this.id = id;
		this.succespercentage = succespercentage;
		this.color = color;
		this.name = name;
	}

	public ToTutorial() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSuccespercentage() {
		return succespercentage;
	}

	public void setSuccespercentage(int succespercentage) {
		this.succespercentage = succespercentage;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
