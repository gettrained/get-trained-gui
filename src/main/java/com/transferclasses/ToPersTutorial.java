package com.transferclasses;

import java.io.Serializable;
import java.util.Date;

public class ToPersTutorial implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	private int tutorialId;
	private String state;
	private String lastseen;
	private Date datefinished;
	private Date datecreated;
	private int usedtime;

	public ToPersTutorial() {

	}

	public ToPersTutorial(int id,int tutorialId, String state, String lastseen, Date datefinished, Date datecreated,
			int usedtime) {
		this.id = id;
		this.tutorialId = tutorialId;
		this.state = state;
		this.lastseen = lastseen;
		this.datefinished = datefinished;
		this.datecreated = datecreated;
		this.usedtime = usedtime;
	}
	
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTutorialId() {
		return tutorialId;
	}

	public void setTutorialId(int tutorialId) {
		this.tutorialId = tutorialId;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getLastseen() {
		return lastseen;
	}

	public void setLastseen(String lastseen) {
		this.lastseen = lastseen;
	}

	public Date getDatefinished() {
		return datefinished;
	}

	public void setDatefinished(Date datefinished) {
		this.datefinished = datefinished;
	}

	public Date getDatecreated() {
		return datecreated;
	}

	public void setDatecreated(Date datecreated) {
		this.datecreated = datecreated;
	}

	public int getUsedtime() {
		return usedtime;
	}

	public void setUsedtime(int usedtime) {
		this.usedtime = usedtime;
	}

}
