package com.transferclasses;

import java.io.Serializable;
import java.util.LinkedList;

public class ToQuestions implements Serializable {
	private static final long serialVersionUID = 1L;
	private LinkedList<ToQuestion>questions;
		
	public ToQuestions() {
		
	}
	

	public ToQuestions(LinkedList<ToQuestion> questions) {
		this.questions=questions;
	}
	
	public LinkedList<ToQuestion> getQuestions() {
		return questions;
	}
	public void setQuestions(LinkedList<ToQuestion> questions) {
		this.questions = questions;
	}
	
}
