package com.transferclasses;

import java.io.Serializable;

public class PostPausePersonalAnswer implements Serializable {
	private static final long serialVersionUID = 1L;
	private int personalTutorialID;
	private int usedtime;
	private String lastseen;

	public PostPausePersonalAnswer() {
		super();
	}

	public PostPausePersonalAnswer(int personalTutorialID, int usedtime, String lastseen) {
		super();
		this.personalTutorialID = personalTutorialID;
		this.usedtime = usedtime;
		this.lastseen = lastseen;
	}

	public int getPersonalTutorialID() {
		return personalTutorialID;
	}

	public void setPersonalTutorialID(int personalTutorialID) {
		this.personalTutorialID = personalTutorialID;
	}

	public int getUsedtime() {
		return usedtime;
	}

	public void setUsedtime(int usedtime) {
		this.usedtime = usedtime;
	}

	public String getLastseen() {
		return lastseen;
	}

	public void setLastseen(String lastseen) {
		this.lastseen = lastseen;
	}

}
