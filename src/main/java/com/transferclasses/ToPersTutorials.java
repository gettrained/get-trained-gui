package com.transferclasses;

import java.io.Serializable;
import java.util.LinkedList;

public class ToPersTutorials implements Serializable {
	private static final long serialVersionUID = 1L;
	private LinkedList<ToPersTutorial> perstutorials = new LinkedList<>();
	
	public LinkedList<Integer>getPersTutorialIdsPerState(String state){
		LinkedList<Integer> persTutorial= new LinkedList<>();		
		for(int i=0; i<perstutorials.size();i++) {
			if(perstutorials.get(i).getState().toLowerCase().equals(state.toLowerCase())&&persTutorial.indexOf(perstutorials.get(i).getTutorialId())==-1) {
				persTutorial.add(perstutorials.get(i).getTutorialId());
			}
		}
		return persTutorial;
	}
	
	public LinkedList<ToPersTutorial> getPersTutorialsPerState(String state) {
		LinkedList<ToPersTutorial> persTutorial= new LinkedList<>();
		for(int i=0; i<perstutorials.size();i++) {
			if(perstutorials.get(i).getState().toLowerCase().equals(state.toLowerCase())) {
				persTutorial.add(perstutorials.get(i));
			}
		}
		return persTutorial;
	}

	public LinkedList<ToPersTutorial> getPerstutorials() {
		return perstutorials;
	}

	public void setPerstutorials(LinkedList<ToPersTutorial> perstutorials) {
		this.perstutorials = perstutorials;
	}

	public ToPersTutorials() {
	}

	public ToPersTutorials(LinkedList<ToPersTutorial> perstutorials) {
		this.perstutorials = perstutorials;
	}

	public ToPersTutorial getPartialPersTutorialByTutorialID(int tutorial_id) {
		LinkedList<ToPersTutorial>partialTut=getPersTutorialsPerState("Partial");
		for(ToPersTutorial item:partialTut) {
			if(item.getTutorialId()==tutorial_id) {
				return item;
			}
		}
		return null;
	}

}
