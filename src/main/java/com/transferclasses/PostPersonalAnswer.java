package com.transferclasses;

import java.io.Serializable;

public class PostPersonalAnswer implements Serializable {
	private static final long serialVersionUID = 1L;
	private int personaltutorialID;
	private int answerID;
	private boolean selected;
	
	
	
	public PostPersonalAnswer() {
		super();
	}
	
	public PostPersonalAnswer(int personaltutorialID, int answerID, boolean selected) {
		super();
		this.personaltutorialID = personaltutorialID;
		this.answerID = answerID;
		this.selected = selected;
	}
	public int getPersonaltutorialID() {
		return personaltutorialID;
	}
	public void setPersonaltutorialID(int personaltutorialID) {
		this.personaltutorialID = personaltutorialID;
	}
	public int getAnswerID() {
		return answerID;
	}
	public void setAnswerID(int answerID) {
		this.answerID = answerID;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	
}
