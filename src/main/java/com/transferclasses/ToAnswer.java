package com.transferclasses;

import java.io.Serializable;

public class ToAnswer implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	private boolean isCorrect;
	private String answerText;
	
	public ToAnswer() {
		
	}
	

	
	public ToAnswer(int id, boolean isCorrect, String answerText) {
		super();
		this.id = id;
		this.isCorrect = isCorrect;
		this.answerText = answerText;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public boolean isCorrect() {
		return isCorrect;
	}

	public void setCorrect(boolean isCorrect) {
		this.isCorrect = isCorrect;
	}

	public String getAnswerText() {
		return answerText;
	}

	public void setAnswerText(String answerText) {
		this.answerText = answerText;
	}
	
}
