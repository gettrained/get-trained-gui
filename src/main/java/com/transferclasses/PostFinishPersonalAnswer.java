package com.transferclasses;

import java.io.Serializable;

public class PostFinishPersonalAnswer implements Serializable {
	private static final long serialVersionUID = 1L;

	private int personalTutorialID;
	private int usedtime;

	public PostFinishPersonalAnswer() {
		super();
	}

	public PostFinishPersonalAnswer(int personalTutorialID, int usedtime) {
		super();
		this.personalTutorialID = personalTutorialID;
		this.usedtime = usedtime;
	}

	public int getPersonalTutorialID() {
		return personalTutorialID;
	}

	public void setPersonalTutorialID(int personalTutorialID) {
		this.personalTutorialID = personalTutorialID;
	}

	public int getUsedtime() {
		return usedtime;
	}

	public void setUsedtime(int usedtime) {
		this.usedtime = usedtime;
	}

}
