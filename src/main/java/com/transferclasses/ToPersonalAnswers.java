package com.transferclasses;

import java.io.Serializable;
import java.util.LinkedList;

public class ToPersonalAnswers implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private LinkedList<ToPersonalAnswer> personalanswers;
	
	public ToPersonalAnswers() {
		super();
	}
	public ToPersonalAnswers(LinkedList<ToPersonalAnswer> personalanswers) {
		super();
		this.personalanswers = personalanswers;
	}

	public LinkedList<ToPersonalAnswer> getPersonalanswers() {
		return personalanswers;
	}

	public void setPersonalanswers(LinkedList<ToPersonalAnswer> personalanswers) {
		this.personalanswers = personalanswers;
	}
	
	
	
}
