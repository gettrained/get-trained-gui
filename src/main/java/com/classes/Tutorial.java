package com.classes;

import java.util.Date;
import java.util.LinkedList;

import com.comparator.ComparatorQuestion;
import com.transferclasses.PostPersonalAnswer;
import com.transferclasses.ToPersonalAnswer;
import com.transferclasses.ToTutorial;

public class Tutorial {
	private int id;
	private int perstut_id;
	private int succespercentage;
	private String color;
	private String name;
	private String state;
	private LinkedList<Walkthrough>walkthroughs;
	private LinkedList<Question>questions;
	private LinkedList<Material>materials;
	private Date startTutorialUser;//Zeitpunkt an dem der User das aktuelle Tutorial gestartet hat.
	private int usedTime;//UsedTime, die in der Datenbank gespeichert ist.
	private String lastSeen;
	
	public Tutorial(ToTutorial tutorial, String state) {
		this.id=tutorial.getId();
		this.succespercentage=tutorial.getSuccespercentage();
		this.color=tutorial.getColor();
		this.name=tutorial.getName();
		this.state=state;		
		this.walkthroughs=new LinkedList<>();
		this.questions=new LinkedList<>();
		this.materials=new LinkedList<>();
		
	}
	public void addMaterial(Material material) {
		materials.add(material);
	}
	public void addQuestion(Question question) {
		questions.add(question);
	}
	public Question getQuestionByOrder(int order) {
		for(int i=0;i<questions.size();i++) {
			if(questions.get(i).getOrder()==order) {
				return questions.get(i);
			}
		}
		return null;
	}
	public Material getMaterialByOrder(int order) {
		if(order==0) {
			order=1;
		}
		for(int i=0;i<materials.size();i++) {
			if(materials.get(i).getOrder()==order) {
				return materials.get(i);
			}
		}
		return null;
	}
	public void setAnswersByIds(String[]s_answers,String[]s_ids) {
		for(Question question:questions) {
			if(question.isAnswerIncluded(Integer.parseInt(s_ids[0]))) {
				for(int i=0;i<s_ids.length;i++) {
					question.setAnswerSelected(Integer.parseInt(s_ids[i]), Boolean.parseBoolean(s_answers[i]));
				}
			}
		}
	}
	public boolean setAnswerToQuestion(int id,boolean answer) {
		for(Question question:questions) {
			if(question.includesAnswerID(id)) {
				return question.setAnswerSelected(id, answer);
			}
		}
		return false;
	}
	public LinkedList<PostPersonalAnswer>getPostPersonalAnswers(int untilOrder){
		LinkedList<PostPersonalAnswer>post_answers=new LinkedList<>();
		for(Question question:getQuestions_sorted()) {			
			post_answers.addAll(question.getPostPersonalAnswers(perstut_id));//speichert alle PersonalAnswers ab, auch wenn nach der ersten frage keine mehr beantwortet worden sind.
			if((question.getOrder()>=untilOrder)&&(untilOrder!=-1)) {
				//Question mit Order== tilOrder wird ebenfalls gespeichert.
				// >= um fehlerhafte Daten, wie bei fehlen einer Orderzahl zu verhindern(zB 1 2 3 5 6)
				//untilOrder=-1 wenn alle Antworten gespeichert werden sollen.
				break;
			}
		}
		return post_answers;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getSuccespercentage() {
		return succespercentage;
	}
	public void setSuccespercentage(int succespercentage) {
		this.succespercentage = succespercentage;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public LinkedList<Walkthrough> getWalkthroughs() {
		return walkthroughs;
	}
	public void setWalkthroughs(LinkedList<Walkthrough> walkthroughs) {
		this.walkthroughs = walkthroughs;
	}
	public void addWalkthrough(String text, int tutorial_id, int reached_percentage ) {
		this.walkthroughs.add(new Walkthrough(text, tutorial_id,reached_percentage));
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}

	public LinkedList<Question> getQuestions() {
		
		return questions;
	}
	public LinkedList<Question> getQuestions_sorted(){
		questions.sort(new ComparatorQuestion());
		return questions;
	}

	public void setQuestions(LinkedList<Question> questions) {
		this.questions = questions;
	}

	public LinkedList<Material> getMaterial() {
		return materials;
	}

	public void setMaterial(LinkedList<Material> materials) {
		this.materials = materials;
	}
	public int getPerstut_id() {
		return perstut_id;
	}
	public void setPerstut_id(int perstut_id) {
		this.perstut_id = perstut_id;
	}
	public Date getStartTutorialUser() {
		return startTutorialUser;
	}
	public void setStartTutorialUser(Date startTutorialUser) {
		this.startTutorialUser = startTutorialUser;
	}
	public int getNewUsedTime() {//int kann die Zeit von 24,855... Stunden speichern		
		long time=new java.util.Date().getTime()-startTutorialUser.getTime();
		
		return (int)time+usedTime;
	}
	public double getLastSeen_Seconds() {
		if(lastSeen.contains(".")) {
			return Double.parseDouble(lastSeen);
		}else {
			return -1;
		}		
	}
	public int getLastSeen_Order() {
		if(lastSeen.contains(".")) {
			return 0;
		}else {
			try {
				return Integer.parseInt(lastSeen)+1;
			}catch (NumberFormatException ex){
				return -1;
			}
		}
	}
	public void setLastSeen(String lastSeen) {
		this.lastSeen = lastSeen;
	}

	public int getUsedTime() {
		return usedTime;
	}
	public void setUsedTime(int usedTime) {
		this.usedTime = usedTime;
	}
	public int setPersonalAnswer(ToPersonalAnswer toPersonalAnswer) {//setzen der PersonalAnswer zur richtigen Antwort und r�ckgabewert ist die Order der Frage zu der sie geh�rt.
		for(Question question:questions) {
			if(question.setAnswerSelected(toPersonalAnswer.getAnswerID(), toPersonalAnswer.isSelected())) {
				return question.getOrder();
			}
		}
		return -1;
	}

	public boolean isTutorialPassed() {
		boolean passed=false;
		for(Walkthrough w:walkthroughs) {
			if(w.getReached_percentage()>=getSuccespercentage()) {
				passed=true;
			}
		}
		return passed;
	}
}
