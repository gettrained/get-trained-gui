package com.classes;

import java.util.LinkedList;

import com.transferclasses.PostPersonalAnswer;

public class Question {
	private LinkedList<Answer>answers;
	private String question_text;
	private int order;
	private String bytestream;
	
	public Question(LinkedList<Answer>answers,String question_text,int order, String bytestream) {
		this.answers=answers;
		this.question_text=question_text;
		this.order=order;
		this.bytestream=bytestream;
	}
	
	public LinkedList<Answer> getAnswers() {
		return answers;
	}
	public void setAnswers(LinkedList<Answer> answers) {
		this.answers = answers;
	}
	public boolean setAnswerSelected(int id, boolean selected) {
		for(Answer answer:answers) {
			if(answer.getId()==id) {
				answer.setSelected(selected);
				return true;
			}
		}
		return false;
	}
	public boolean isAnswerIncluded(int id) {
		for(Answer answer:answers) {
			if(answer.getId()==id) {
				return true;
			}
		}
		return false;
	}
	public int getAmountOfCorrectAnswers() {
		int count=0;
		for(Answer answer:answers) {
			if(answer.isCorrect()) {
				count++;
			}
		}
		return count;
	}
	public String getQuestion_text() {
		return question_text;
	}
	public void setQuestion_text(String question_text) {
		this.question_text = question_text;
	}
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	public String getByteStream() {
		return bytestream;
	}
	public void setByteStream(String bytestream) {
		this.bytestream = bytestream;
	}

	public LinkedList<PostPersonalAnswer> getPostPersonalAnswers(int perstut_id) {
		LinkedList<PostPersonalAnswer>post_answers=new LinkedList<>();
		for(Answer answer:answers) {
			post_answers.add(new PostPersonalAnswer(perstut_id, answer.getId(), answer.isSelected()));
		}
		return post_answers;
	}
	public boolean includesAnswerID(int answerID) {
		boolean includes =false;
		for(int i=0;i<answers.size();i++) {
			includes=includes||(answers.get(i).getId()==answerID);
		}
		return includes;
	}
}
