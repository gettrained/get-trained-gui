package com.classes;

import java.util.LinkedList;

import com.comparator.ComparatorQuestion;
public class Result {
	private LinkedList<Question>questions;
	private String tutorial_name;
	private String tutorial_color;
	private int personal_tutorial_id;
	
	public Result() {
		questions=new LinkedList<>();
	}
	
	public Result(LinkedList<Question>questions) {
		this.setQuestions(questions);
	}

	public LinkedList<Question> getQuestions() {
		questions.sort(new ComparatorQuestion());
		return questions;
	}

	public void setQuestions(LinkedList<Question> questions) {
		this.questions = questions;
	}

	public String getTutorial_name() {
		return tutorial_name;
	}

	public void setTutorial_name(String tutorial_name) {
		this.tutorial_name = tutorial_name;
	}

	public String getTutorial_color() {
		return tutorial_color;
	}

	public void setTutorial_color(String tutorial_color) {
		this.tutorial_color = tutorial_color;
	}

	public int getPersonal_tutorial_id() {
		return personal_tutorial_id;
	}

	public void setPersonal_tutorial_id(int personal_tutorial_id) {
		this.personal_tutorial_id = personal_tutorial_id;
	}
}
