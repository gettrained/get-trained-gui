package com.classes;


public class Material {
    private String materialtype;
    private int order;
    private String text;
    private String material;
    

    public Material(String materialtype,int order,String text,String material) {
    	this.materialtype=materialtype;
    	this.order=order;
    	this.text=text;
    	this.setMaterial(material);
    }
	public String getMaterialtype() {
		return materialtype;
	}
	public void setMaterialtype(String materialtype) {
		this.materialtype = materialtype;
	}
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getMaterial() {
		return material;
	}
	public void setMaterial(String material) {
		this.material = material;
	}
	
}
