package com.classes;

public class Answer {
	private int id;
	private boolean isSelected;
	private String text;
	private boolean isCorrect;
	
	public Answer(int id,boolean isSelected, String text, boolean isCorrect) {
		this.id=id;
		this.isSelected=isSelected;
		this.isCorrect=isCorrect;
		this.text=text;
	}
	
	public boolean isSelected() {
		return isSelected;
	}
	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public boolean isCorrect() {
		return isCorrect;
	}
	public void setCorrect(boolean isCorrect) {
		this.isCorrect = isCorrect;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
