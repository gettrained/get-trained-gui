package com.classes;

public class Walkthrough {
	private String text;
	private int tutorial_id;
	private int reached_percentage;
	
	public Walkthrough(String text, int tutorial_id,int reached_percentage) {
		this.text=text;
		this.tutorial_id=tutorial_id;
		this.reached_percentage=reached_percentage;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getTutorial_id() {
		return tutorial_id;
	}

	public void setTutorial_id(int tutorial_id) {
		this.tutorial_id = tutorial_id;
	}

	public int getReached_percentage() {
		return reached_percentage;
	}

	public void setReached_percentage(int reached_percentage) {
		this.reached_percentage = reached_percentage;
	}
}
