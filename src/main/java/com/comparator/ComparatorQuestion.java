package com.comparator;

import java.util.Comparator;

import com.classes.Question;

public class ComparatorQuestion implements Comparator<Question> {

	@Override
	public int compare(Question o1, Question o2) {
		if(o1.getOrder()<o2.getOrder()) {
			return -1;
		}else if(o1.getOrder()==o2.getOrder()) {
			return 0;
		}else {
			return 1;
		}
	}



}
