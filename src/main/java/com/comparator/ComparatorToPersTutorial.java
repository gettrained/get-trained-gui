package com.comparator;

import java.util.Comparator;

import com.transferclasses.ToPersTutorial;

public class ComparatorToPersTutorial implements Comparator<ToPersTutorial> {

	@Override
	public int compare(ToPersTutorial o1, ToPersTutorial o2) {
		if(o1.getTutorialId()<o2.getTutorialId()) {
			return -1;
		}else if(o1.getTutorialId()==o2.getTutorialId()) {
			return 0;
		}else{
			return 1;
		}
	}

}
