$(document).ready(function(){
	$(".tablink").ready(function(){
		for(var i=0; i<$(".tablink").length;i++){
			var default_tap=$(".tablink")[i].id.split('_').slice(-1)[0];
			if(default_tap=='open'){
				$(".tablink")[i].click();
			}
		}
	});
	$(".container_tutorial").hover(
			function(){
				$(this).css("background-color","#bbb");
				/*var background=$(this).css("background-color");
				var foreground=$(this).css("color");
				$(this).css("background-color",foreground);
				$(this).css("color",background);*/
			},
			function(){
				$(this).css("background-color","#fff");
				/*var background=$(this).css("background-color");
				var foreground=$(this).css("color");
				$(this).css("background-color",foreground);
				$(this).css("color",background);*/
			});
	$("i.rotate").click(function(){		
		var tab_number=$(this)[0].id.split('_').slice(-2)[0];
		var id=$(this)[0].id.split('_').slice(-1)[0];
		var walkthroughs=$(".div_walkthroughs_"+tab_number+"_"+id);
		for(var i=0;i<walkthroughs.length;i++){
			walkthroughs[i].classList.toggle("show");
		}
		
		$(this).toggleClass("down");
	});	
    $(".start").click(function(){
		if($(this)[0].id.split('_')[2]!="continue"){
	        window.open("startTutorial","_self");  
		}else{
			window.open("continueTutorial","_self");
		}  	
    });
    $(".load").click(function(){
    	
    	var id=$(this)[0].id.split('_').slice(-1)[0];
    	var tab_number=$(this)[0].id.split('_').slice(-2)[0];		
    	actual_div=$('#button_tutorial_load_'+tab_number+'_'+id)[0].parentElement;
    	var hasTutorialPartialVersion=false;
    	if(tab_number!=1){
    		var partial_tutorials=$("#div_partial .button_tutorial");
    		for(var i=0;i<partial_tutorials.length;i++){
    			if(id==partial_tutorials[i].id.split('_').slice(-1)[0]){
    				hasTutorialPartialVersion=true;
    			}
    		}
    	}
    	if(!hasTutorialPartialVersion){
    		
    		$("#img_loading_gif_"+tab_number+'_'+id).css('visibility', 'visible');  
    		$("#img_loading_gif_3_"+tab_number+'_'+id).css('visibility', 'visible');
    		$('.load').prop('disabled', true);
    		$('.button_result').prop('disabled', true)
    		$(this).classname+=" active";	
    		var xhttp=new XMLHttpRequest();
    		xhttp.onreadystatechange=function(){
    			if(this.readyState==4 && this.status==200){
    				if(this.responseText=="successful"){
    					$("#img_loading_gif_"+tab_number+'_'+id).css('visibility', 'hidden');
    					$("#img_loading_gif_3_"+tab_number+'_'+id).css('visibility', 'hidden');
    					for(var i=0;i<actual_div.children.length;i++){
    						if(actual_div.children[i].classList.contains("start")){
    							actual_div.children[i].style.display="block";
    							$('#'+actual_div.children[i].id.replace('_'+tab_number+'_','_3_'+tab_number+'_'))[0].style.display="block";
    						}
    						if(actual_div.children[i].classList.contains("load")){
    							actual_div.children[i].style.display="none";
    							$('#'+actual_div.children[i].id.replace('_'+tab_number+'_','_3_'+tab_number+'_'))[0].style.display="none";
        						
    						}
    					}
    				}else{
    					$('#div_modal p')[0].innerHTML=this.responseText;
    					$('#div_modal')[0].style.display=block;
    				}
    			}
    		};
    		xhttp.open("GET","loadTutorial_body?tutorial_id="+id+"&group_number="+tab_number,true);
    		xhttp.send();
    	}else{
    		$("#div_modal")[0].style.display="block";
    	}
    });
    $(".button_result").click(function(){    	
    	var ids=$(this)[0].id.split('_').slice(-2);
    	window.open("result?tutorial_id="+ids[0]+'&persTut_id='+ids[1], "_self")
    });
    
});

function openTab(evt, tabName){
	 var i, tabcontent, tablinks;
	    tabcontent = document.getElementsByClassName("tabcontent");
	  	for (i = 0; i < tabcontent.length; i++) {
	    	tabcontent[i].style.display = "none";
	    }
		
	    tablinks = document.getElementsByClassName("tablink");
	    for (i = 0; i < tablinks.length; i++) {
	        tablinks[i].className = tablinks[i].className.replace(" active", "");
	    }
	    document.getElementById(tabName).setAttribute("style", "display:block;");
	    evt.currentTarget.className += " active";
	}
