$("document").ready(function(){	
	$("#i_arrow_right").click(function(){		
		var order=$(".input_checkbox")[0].id.split('_').slice(-2)[0];
		var newOrder=parseInt(order,10)+1;
		var inputs=$(".input_checkbox");
		var ajax_problems=false;
		var oneSelected=false
		for(var i=0;i<inputs.length;i++){
			oneSelected=oneSelected||inputs[i].checked;
		}
		if(oneSelected){
			for(var i=0;i<inputs.length;i++){
				var answer=inputs[i].checked;
				var id=inputs[i].id.split('_').slice(-1)[0];
				var xhttp=new XMLHttpRequest();
				xhttp.onreadystatechange=function(){
					if(this.readyState==4 && this.status==200){
						ajax_problems=ajax_problems||this.responseText;
					}
				};
				xhttp.open("GET","sendAnswer?answer_id="+id+"&answer="+answer,true);
				xhttp.send();
			}
			if(!ajax_problems){
				window.open("question?order="+newOrder,"_self");
			}else{
				//modal error
			}
		}else{
			//Modal 			
			$("#div_modal")[0].style.display="block";			
		}
		
	});
	$("#i_arrow_left").click(function(){
		var order=$(".input_checkbox")[0].id.split('_').slice(-2)[0];
		var newOrder=parseInt(order,10)-1;
		var inputs=$(".input_checkbox");
		var ajax_problems=false;
		for(var i=0;i<inputs.length;i++){
			var id=inputs[i].id.split('_').slice(-1)[0];
			var answer=inputs[i].checked;
			var xhttp=new XMLHttpRequest();
    		xhttp.onreadystatechange=function(){
    			if(this.readyState==4 && this.status==200){
    				ajax_problems=ajax_problems||this.responseText;
    			}
    		};
    		xhttp.open("GET","sendAnswer?answer_id="+id+"&answer="+answer,true);
    		xhttp.send();
		}
		if(!ajax_problems){
			window.open("question?order="+newOrder+"&answers=&ids=","_self");	
		}else{
			//modal Error
		}
		 
		
	});
	$("#i_exit_tutorial").click(function(){
		var order=$(".input_checkbox")[0].id.split('_').slice(-2)[0];
		window.open("exitQuestion?order="+order,"_self");
	});
});