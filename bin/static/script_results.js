$("document").ready(function(){
	withinviewport.defaults.top=-document.documentElement.clientHeight;
	withinviewport.defaults.bottom=-document.documentElement.clientHeight;
	withinviewport.defaults.sides='top bottom';
	
	$("#i_arrow_right").click(function(){		
		var divs=$(".div_main");
		var div;
		for(var i=0;i<divs.length;i++){
			if(withinviewport(divs[i])){
				div=divs[i];				
				break;
			}
		}
		if(div!=null){
		var order=div.id.split('_').slice(-1)[0];
		var new_order=parseInt(order,10)+1;
		var new_id= "div_main_"+new_order;
		document.getElementById(new_id).scrollIntoView();
		}   	
	});
	$("#i_arrow_left").click(function(){
		var divs=$(".div_main");
		var div;
		for(var i=divs.length-1;i>=0;i--){
			if(withinviewport(divs[i])){
				div=divs[i];
				
				break;
			}
		}
		if(div!=null){
		var order=div.id.split('_').slice(-1)[0];
		var new_order=parseInt(order,10)-1;
		var new_id= "div_main_"+new_order;
		document.getElementById(new_id).scrollIntoView();
		}
	})
	$("#i_save").click(function(){
		$('#div_modal')[0].style.display="block";
		window.open("getPDF","_self");		
	});
	$("#i_exit_tutorial").click(function(){
		window.open("tutorials","_self");
	});
});

